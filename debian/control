Source: raku-librarycheck
Maintainer: Debian Rakudo Maintainers <pkg-rakudo-devel@lists.alioth.debian.org>
Uploaders: Dominique Dumont <dod@debian.org>
Section: interpreters
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-raku,
               rakudo (>= 2022.07-2)
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl6-team/modules/raku-librarycheck
Vcs-Git: https://salsa.debian.org/perl6-team/modules/raku-librarycheck.git
Homepage: https://github.com/jonathanstowe/LibraryCheck
Rules-Requires-Root: no

Package: raku-librarycheck
Architecture: any
Depends: ${misc:Depends},
         ${raku:Depends},
         rakudo (>= 2022.07-2)
Description: Determine whether a shared library is available to be loaded by Raku
 This module provides a mechanism that will determine whether a named
 shared library is available and can be used by NativeCall.
 .
 It exports a single function library-exists that returns a boolean to
 indicate whether the named shared library can be loaded and used.
